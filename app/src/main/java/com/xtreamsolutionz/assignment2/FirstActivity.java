package com.xtreamsolutionz.assignment2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FirstActivity extends AppCompatActivity {

    private EditText txtName,txtEmail,txtPassword,txtPhone,txtDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        initWidgets ();
        Button btnReg=(Button) findViewById ( R.id.btn_signup );
        btnReg.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                if(isValidateInputs ()){
                    Intent intent = new Intent ( FirstActivity.this,SecondActivity.class );
                    intent.putExtra ("name",txtName.getText ());
                    intent.putExtra ("email",txtEmail.getText ());
                    intent.putExtra ("description",txtDescription.getText ());
                    intent.putExtra ("phonenumber",txtPhone.getText ());
                    startActivity ( intent );
                }
            }
        } );
    }
    private void initWidgets(){
        txtName=(EditText) findViewById ( R.id.txt_name );
        txtEmail=(EditText) findViewById ( R.id.txt_email );
        txtPassword=(EditText) findViewById ( R.id.txt_password );
        txtPhone=(EditText) findViewById ( R.id.txt_phone_number );
        txtDescription=(EditText) findViewById ( R.id.txt_description );
    }

    private boolean isValidateInputs(){
        if(txtName.getText ().equals ( "" )){
           // Toast.makeText ( this,"Please enter name",Toast.LENGTH_LONG ).show ();
            txtName.setError ( "Please enter name" );
            return false;
        }if(txtEmail.getText ().equals ( "" )){
            txtEmail.setError ( "Please enter valid email" );

          //  Toast.makeText ( this,"Please enter name",Toast.LENGTH_LONG ).show ();
            return false;
        }if(txtPassword.getText ().equals ( "" )){
           // Toast.makeText ( this,"Please enter name",Toast.LENGTH_LONG ).show ();
            txtEmail.setError ( "Please enter password" );
            return false;
        }if(txtPhone.getText ().equals ( "" )){
          //  Toast.makeText ( this,"Please enter name",Toast.LENGTH_LONG ).show ();
            txtEmail.setError ( "Please enter phone number" );
            return false;
        }
        return true;
    }

}
