package com.xtreamsolutionz.assignment2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {
    private EditText txtName,txtEmail,txtPhone,txtDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_second );
        initWidgets ();
        Intent intent = getIntent ();
        txtName.setText ( intent.getStringExtra (  "name") );
        txtEmail.setText ( intent.getStringExtra (  "email") );
        txtPhone.setText ( intent.getStringExtra (  "phonenumber") );
        txtDescription.setText ( intent.getStringExtra (  "description") );
        Button btnClose=(Button) findViewById ( R.id.btn_close );
        btnClose.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                finish ();
            }
        } );


    }
    private void initWidgets(){
        txtName=(EditText) findViewById ( R.id.txt_name );
        txtEmail=(EditText) findViewById ( R.id.txt_email );

        txtPhone=(EditText) findViewById ( R.id.txt_phone_number );
        txtDescription=(EditText) findViewById ( R.id.txt_description );
    }
}
